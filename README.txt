
Gallery Assist for the Cooliris Viewer
--------------------------------------

"Gallery Assist for the Cooliris Viewer/Plugin" is a submodule from Gallery Assist module and provides interactive slideshows (3D Wall) of images from Gallery Assist galleries using the Cooliris services and so extends the Gallery Assist module with two new points of visualization.
What you need to use it?

You need the module Gallery Assist and you can combine with the functionalities 
of the module Gallery Assist ImageCache for more flexibility with preset formats 
and with the module Gallery Assist Lightboxes wich allow the use of some lightboxes. 
(highslide, prettyPhoto, fancybox, colorbox, shadowbox, litebox, lightbox2).


What does Gallery Assist for the Cooliris Viewer
------------------------------------------------
Shows your galleries Through a integrated player (embeded).
    This display method allow you to customize the viewer acording your corporate design e.g. background color, sizes etc etc. In this case the visitor does not leave your website and surf through the 3D-wall between your images.

Through linking Piclens.
    Here the visitor leaves your website and surf on the 3D-wall between your images through the website of Cooliris. A way very multimedialic and pretty!

The functionality can be assigned only to node types with Gallery Assist assigment.
    The administrators or webmasters can assign the colliris funtionality to each node type with Gallery Assist assigment and can set the default variables to the cooliris configuration.
    Users with "create galleries" permissions can decide, wich method (embed, piclens) for the teaser- and/or for the page-view are to be used.
    Teaser and Page views can be configured separately for each node from each content type with Gallery Assist assigment.


Links
-----
Livedemo: http://simple.puntolatinoclub.de/category/gallery-assist/examples/cooliris


Install
-------
Nothing expectacular. Copy and activate so as by the others modules and 
over Gallery Assist Extras assign the Cooliris functionality to the content types
with Gallery Assist assigment.


"* Cooliris is a trademark of Cooliris, Inc. Gallery Assist for the Cooliris plugin is not affiliated nor endorsed by Cooliris, Inc"


Maintainer
----------
The Gallery Assist for the Cooliris Viewer module was originally developped by:
Juan Carlos Morejon Caraballo

Current maintainer:
Juan Carlos Morejon Caraballo
